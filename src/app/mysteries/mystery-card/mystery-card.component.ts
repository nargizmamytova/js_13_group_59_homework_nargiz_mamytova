import { Component, Input } from '@angular/core';
import { Question } from '../../shared/question.model';
import { AnswerBackgroundDirective } from '../../directives/answer-background.directive';
import { QuestionService } from '../../shared/question.service';

@Component({
  selector: 'app-mystery-card',
  templateUrl: './mystery-card.component.html',
  styleUrls: ['./mystery-card.component.css']
})
export class MysteryCardComponent{
  @Input() inputAnswer = '';
  @Input() question!: Question;
  modalOpen = false;

constructor(private questionService: QuestionService) {
}
  getAnswer() {
    if(this.inputAnswer === this.question.answer){
      this.question.responseStatus = 'верный';
      this.questionService.getTries()
    }else {
      this.question.responseStatus = 'неверный'
    }
  }
  openCheckoutModal(){
    this.modalOpen = true;
  }
  closeCheckOutModal(){
    this.modalOpen = false
  }

}
