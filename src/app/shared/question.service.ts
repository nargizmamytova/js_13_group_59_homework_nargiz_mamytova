import { Injectable } from '@angular/core';
import { Question } from './question.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  tries = 0;
   private questions: Question[] = [
    new Question
    ('Два мощных имеет клыка, Два ласта и два плавника,Но этого дядю не трожь,  Прилёг отдохнуть толстый… ',
      'моржь',
      'Не боится холода',
      'нет ответа'),
    new Question
    ('Для людей мы как загадки, Но не злые мы ребятки,  Не сыграем с вами в прятки, Потому что мы…',
      'косатка',
      'Из семейства китовых',
      'нет ответа'),
    new Question
    ('Он на севере живёт,Громким голосом ревёт, В цирке мяч ловить не лень — Там работает…',
      'тюлень',
      'Выступает в цирке',
      'нет ответа'),
  ];

  getQuestion(){
   return  this.questions.slice();
  }
  getTries(){
    this.tries = +1
  }

}
