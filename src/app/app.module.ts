import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { MysteriesComponent } from './mysteries/mysteries.component';
import { MysteryCardComponent } from './mysteries/mystery-card/mystery-card.component';
import { QuestionService } from './shared/question.service';
import { ModalComponent } from './ui/modal/modal.component';
import { AnswerBackgroundDirective } from './directives/answer-background.directive';
import { AnswerComponent } from './answer/answer.component';


@NgModule({
  declarations: [
    AppComponent,
    MysteriesComponent,
    MysteryCardComponent,
    ModalComponent,
    AnswerBackgroundDirective,
    AnswerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [QuestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
