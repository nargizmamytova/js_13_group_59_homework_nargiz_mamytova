import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../shared/question.service';
import { Question } from '../shared/question.model';

@Component({
  selector: 'app-mysteries',
  templateUrl: './mysteries.component.html',
  styleUrls: ['./mysteries.component.css']
})
export class MysteriesComponent implements OnInit {
  questions!: Question[];
  tries = 0;
  constructor(private questionServices: QuestionService) { }

  ngOnInit(): void {
    this.questions = this.questionServices.getQuestion();
    this.tries = this.questionServices.tries;
  }

}
