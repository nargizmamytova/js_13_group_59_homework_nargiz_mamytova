import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appAnswerBackground]'
})
export class AnswerBackgroundDirective{
  @Input() set appAnswerBackground(responseStatus: string){}

  responseStatus = 'нет ответа'
  constructor(private el: ElementRef) {}

 @HostListener('click') addGreenClass(){
    if( this.responseStatus === 'верный'){
      this.el.nativeElement.classList('green');
    }else if(this.responseStatus === 'неверный'){
      this.el.nativeElement.classList('red');
    }else {
      this.el.nativeElement.classList('');
    }
}




}
